import axios from 'axios'

const port = 4000

export const apiClient = axios.create({
	baseURL: `http://localhost:${port}/app_dev.php/api`
})
